# دریافت کتاب

نسخه‌ی آفلاین کتاب را در قالب‌بندی فایل PDF را [می‌توانید دانلود کنید](./handbook.pdf). البته این فایل PDF در حال‌حاضر، به درستی راست‌چین نیست. اگر فکر می‌کنید می‌توانید این مورد را حل کنید، لطفا از طریق [مخزن گیت‌لب کتاب](https://gitlab.com/GNULand/ubuntu-for-newcomers) به ما اطلاع دهید (:
